#!/usr/bin/python3

from base64 import b64encode, b64decode
from hashlib import sha256
from time import time
from urllib.parse import quote_plus, urlencode
from hmac import HMAC

def generate_sas_token(uri, key, policy_name, expiry=3600):
	ttl = time() + expiry
	#ttl = 1541572701
	sign_key = "%s\n%d" % ((quote_plus(uri)), int(ttl))

	#print ( sign_key )

	#print ( b64decode(key))

	#print ( sign_key.encode('utf-8') )

	#print ( HMAC(b64decode(key), sign_key.encode('utf-8'), sha256).digest() )

	signature = b64encode(HMAC(b64decode(key), sign_key.encode('utf-8'), sha256).digest())

	#print ( signature )

	rawtoken = {
	'sr' :  uri,
	'sig': signature,
	'se' : str(int(ttl))
	}

	if policy_name is not None:
		rawtoken['skn'] = policy_name

		return 'SharedAccessSignature ' + urlencode(rawtoken)

#print (generate_sas_token("shimizudemo.azure-devices.net", "WaQdUZ6H113D6rpk7qFMDohebi7x/b2Esk1DJ0rXsdM=", "registryReadWrite"))
print (generate_sas_token("shimizudemo.azure-devices.net/devices/Simulate01/api-version=2016-11-14", "o80/ugdvbsJDWv8hlsKExcZOQQo+t5HUGeX1ehRHlaw=", "notuse"))
