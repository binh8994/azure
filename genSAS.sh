#!/bin/bash

get_sas_token() {
    local EVENTHUB_URI=$1
    local SHARED_ACCESS_KEY_NAME=$2
    local SHARED_ACCESS_KEY=$3
    local EXPIRY=${EXPIRY:=$((60 * 60 * 24))} # Default token expiry is 1 day

    local ENCODED_URI=$(echo -n $EVENTHUB_URI | jq -s -R -r @uri)
    local TTL=$(($(date +%s) + $EXPIRY))
    local UTF8_SIGNATURE=$(printf "%s\n%s" $ENCODED_URI $TTL | iconv -t utf8)

    local HASH=$(echo -n "$UTF8_SIGNATURE" | openssl sha256 -hmac $SHARED_ACCESS_KEY -binary | base64)
    local ENCODED_HASH=$(echo -n $HASH | jq -s -R -r @uri)

    echo -n "SharedAccessSignature sr=$ENCODED_URI&sig=$ENCODED_HASH&se=$TTL&skn=$SHARED_ACCESS_KEY_NAME"
}

get_sas_token 'shimizudemo.azure-devices.net/devices' 'registryReadWrite' 'WaQdUZ6H113D6rpk7qFMDohebi7x/b2Esk1DJ0rXsdM='

#SharedAccessSignature sr=shimizudemo.azure-devices.net&sig=PXJ9%2Fd%2BPfcL2N2st8xaLEncQYwc%2Br13zfAv6XOttpXE%3D&se=1559815747&skn=iothubowner
#SharedAccessSignature sr=shimizudemo.azure-devices.net&sig=e25IQ3w36on%2B0B%2BEWOmzpJ4RX3zI9qYMY44LJBWEeic%3D&se=1528513531&skn=iothubowner

#SharedAccessSignature sr=shimizudemo.azure-devices.net%2Fdevices%2FSimulate01&sig=P4KseWnJz8wMxVVv7EH77HDe%2B9vIkPF7rFF3HeeSuCo%3D&se=1528514294
#SharedAccessSignature sr=shimizudemo.azure-devices.net%2Fdevices%2FSimulate01&sig=5hU2i7hApipybVxiirkYCy8OCPEvsfHtFaMil0VuEuk%3D&se=1557819595

